<?php

namespace App\Summary;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;



class Summary extends DB
{

    public $id='';

    public $organization_name='';

    public $Summary;


    public function __construct()
    {

        parent::__construct();

    }

    public function index()
    {
        echo "Summery index";

    }
    public function  setData($data=NULL)
    {
        if (array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }
        if (array_key_exists('organization_name',$data))
        {

            $this->organization_name=$data['organization_name'];
        }
        if (array_key_exists('summary',$data))
        {

            $this->summary=$data['summary'];
        }
    }

    public function store()
    {
        $arrData=array($this->organization_name,$this->summary);


        $sql="insert into summary_of_organization(organization_name,summary)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ OrganizationName: $this->organization_name ] , [Summary: $this->summary ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else Message::message("<div id='msg'></div><h3 align='center'>[ OrganizationName: $this-> organization_name] , [summary: $this->summary ] <br> Data Has not  Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }

}// end of BookTitle class