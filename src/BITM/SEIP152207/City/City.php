<?php

namespace App\City;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;



class City extends DB
{

    public $id='';

    public $user_name='';

    public $city_name='';


    public function __construct()
    {

        parent::__construct();

    }

    public function index()
    {
        echo "City index";

    }
    public function  setData($data=NULL)
    {
        if (array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }
        if (array_key_exists('user_name',$data))
        {

            $this->user_name=$data['user_name'];
        }
        if (array_key_exists('city_name',$data))
        {

            $this->city_name=$data['city_name'];
        }
    }
    public function store()
    {
        $arrData=array($this->user_name,$this->city_name);


        $sql="insert into city(user_name,city_name)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
        {
            Message::Message("Data Has been Inseted sucesfully :)");
        }
        else
            Message::Message("Failed! Data Has Not Been Inserted Succesfully");
    }



}// end of BookTitle class