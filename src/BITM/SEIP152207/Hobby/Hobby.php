<?php

namespace App\Hobby;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;



class Hobby extends DB
{

    public $id='';

    public $user_name='';

    public $hobby='';


    public function __construct()
    {

        parent::__construct();

    }
    public function  setData($data=NULL)
    {
        if (array_key_exists('id', $data)) {

            $this->id = $data['id'];
        }
        if (array_key_exists('user_name', $data)) {

            $this->user_name = $data['user_name'];
        }
        if (array_key_exists('hobby', $data)) {

            $this->hobby = $data['hobby'];
        }
    }

        public function store()
    {
        $arrData=array($this->user_name,$this->hobby);


        $sql="insert into hobby(user_name,hobby)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [Hobby: $this->hobby ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [Hobby: $this->hobby ] <br> Data Has not  Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }




}